# omi-workspace-cleaner

Remove outdated _workspace_ data produced by `omi-pipeline` jobs.

_workspace_ data is stored as directories named as GitLab pipeline ids (10 digit number).
GitLab projects triggering the pipeline are located in `https://gitlab.com/open-mobility-indicators/indicators` GitLab group.

This script first queries the GitLab API to get pipeline information related to these projects.
Pipeline information includes (for each project):

- last _nf_ failed pipelines (_nf_ can be changed using `KEEP_MAX_FAILED` env variable, or `--keep_max_failed` command parameter)
- last _ns_ successful pipelines (_ns_ can be changed using `KEEP_MAX_SUCCESS` env variable, or `--keep_max_success` command parameter)
- running pipelines (all of them)

Then the script browses _workspace_ directory one by one, and removes them except if:

- the associated pipeline belongs to the _nf_ failed pipelines to keep
- the associated pipeline belongs to the _ns_ successful pipelines to keep
- the associated pipeline is a running pipeline
- the directory has been created since the script started

## Install

Using a virtualenv:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

## Configure

```bash
cp .env.example .env
```

and adapt `.env` file content to your needs

## Run

```bash
dotenv run python workspace_cleaner.py --debug
```

Script can also be run using `docker` and `docker-compose`
