# OMI cleaner

Projects relative to data cleaning in OMI platform

- [workspaces cleaner](workspaces/README.md): this cleaner service is installed as a _systemd timer service_ on OMI server to insure removing outdated workspace data on a regular basis (this is part of cleaning jobs operated on the server, cf [omi-ops](https://gitlab.com/open-mobility-indicators/omi-ops/#remove-unused-docker-resources) project) 
